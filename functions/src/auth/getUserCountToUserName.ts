
import * as admin from 'firebase-admin'

export default function getUserCountToUserName(userName: string): Promise<FirebaseFirestore.QuerySnapshot> {
 
    return admin.firestore()
      .collection("Users")
      .where("displayName", "==", userName)
      .get()
      
}
