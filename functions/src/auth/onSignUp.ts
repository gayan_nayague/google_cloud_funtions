import * as admin from 'firebase-admin';
// import getUserCountToUserName from './../auth/getUserCountToUserName'

export default function saveSignedUpUserToDb(user: admin.auth.UserRecord): Promise<void | FirebaseFirestore.WriteResult> {

  if (!user.disabled) {

    const saveInDB = {

      "email": user.email,
      "emailVerified": user.emailVerified,
      "phoneNumber": user.phoneNumber,
      "creationTime": user.metadata.creationTime
    }

    return admin.firestore()
      .collection('Users')
      .doc(user.uid)
      .set(saveInDB)
      
  } else {

    console.log("User with uid : " + user.uid + " is disabled")

    return new Promise<void>((reject) => {
      reject();
    })
  }

};