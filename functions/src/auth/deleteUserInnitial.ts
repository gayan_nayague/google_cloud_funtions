import * as admin from 'firebase-admin'

export default function deleteUserInnitial(userId: string): Promise<void> {
    return admin
    .auth()
    .deleteUser(userId)
};