import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import saveSignedUpUserToDb from './auth/onSignUp'
import addDefaultValuesToThePost from './db/User_Post/onCreate'
import deleteUserInnitial from './auth/deleteUserInnitial'
import likeUnlikeToPost from './db/User_Post/onLike'
import commentOnPost from './db/Posts_Comments/onComment'
import likeOnComment from './db/Posts_Comments/onLike'
import deleteThePost from './db/User_Post/onDelete'
import getCategories from './db/Categories/getCategories'
import onProfilePictureUpload from './firebaseStore/User/onProfilePictureUpload'
import commentOnDelete from './db/Posts_Comments/onDelete'
import getUserCountToUserName from './auth/getUserCountToUserName'

const cors = require('cors')({
  origin: true
});

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//     response.send("Hello from Findyo!");
// });

export const postLike = functions.https.onRequest((request, response) => {

  cors(request, response, () => {
    if (request.method === "POST") {

      const likeBody = request.body

      likeUnlikeToPost(likeBody)
        .then(() => {
          response.status(200).end()
        })
        .catch(error => {
          const status = likeBody.like ? 'like' : 'unlike'
          console.error("Error when " + status + " the post", error)

          response.status(500).end()
        })

    } else {
      response.status(405).end()
    }
  })
})
export const postComment = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    //Check whether the request is POST
    if (request.method === "POST") {

      commentOnPost(request.body)
        .then(() => {
          response.status(200).end()
        })
        .catch(error => {
          response.status(500).end()
        })
    } else {
      response.status(405).end()
    }
  })

})
export const commentLike = functions.https.onRequest((request, response) => {
  //console.log("Entered")
  cors(request, response, () => {
    if (request.method === "POST") {
      const likeBody = request.body

      likeOnComment(likeBody)
        .then(() => {
          response.status(200).end()
        })
        .catch(error => {
          const status = likeBody.like ? 'like' : 'unlike'
          console.error('Error when ' + status + ' the comment', error)

          response.status(500).end()
        })
    } else {
      response.status(405).end()
    }
  })
})
export const deletePost = functions.https.onRequest((request, response) => {

  cors(request, response, () => {
    if (request.method === "DELETE") {

      const postId = request.body.postID
      //console.log('Post ID ' + postId)

      deleteThePost(postId)
        .then(() => {
          response.status(200).end()
        })
        .catch(error => {
          console.error('Error deleting comments on the post', error)

          response.status(500).end()
        })
    } else {
      response.status(405).end()
    }
  })

})
export const getAllCategories = functions.https.onRequest((request, response) => {

  if (request.method === 'GET') {

    type Category = {
      CatID: string;
      Name: string;
    }

    const categoriesArr: Category[] = []

    getCategories()
      .then((querySnapshot) => {

        querySnapshot.forEach(doc => {
          const cat: Category = {
            CatID: doc.id,
            Name: doc.data().name_en
          }
          categoriesArr.push(cat)
        })
        response.status(200).send(categoriesArr).end()
      })
      .catch(error => {
        console.error("Error when retriving categories" , error)
        response.status(500).end()
      })
  } else {
    response.status(405).end()
  }
})
export const changeAllProfileURLs = functions.https.onRequest((request, response) => {

  cors(request, response, () => {
    //Check whether the request is POST
    if (request.method === "POST") {
      try {
        const result = onProfilePictureUpload(request.body.UID)
        response.status(200).send(result).end()

      } catch (ex) {
        console.error('Error saving new profile link ', ex)
        response.status(500).end()
      }
    } else {
      response.status(405).end()
    }
  })

})
export const deleteComment = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    if (request.method === "DELETE") {

      const data = request.body

      commentOnDelete(data.CommentID, data.PostID)
        .then(() => {
          response.status(200)
          response.end()
        })
        .catch(error => {
          console.error('Error deleting the comment', error)
          response.status(500).end()
        })
    }
    else {
      response.status(405).end()
    }
  })
})
export const checkUserNameAlreadyExists = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    if (request.method === "POST") {

      const data = request.body

      getUserCountToUserName(data.UserName)
        .then(querySnapshot => {

          const status = querySnapshot.size > 0 ? true : false
          response.status(200).send(status).end()
        })
        .catch(error => {
          console.error("Error geting usrename count",error)
          response.status(500).send(error).end()
        })
    }
    else{
      response.status(405).end()
    }
  })
})

admin.initializeApp(functions.config().firebase);

//Save the authenticated users in to DB
exports.saveSignedUpUserToDb =  functions.auth.user().onCreate(async  user => {
  return await saveSignedUpUserToDb(user)
    .catch(async error_1 => {
      console.error('Failed to save signed up user' ,error_1)
      //Delete user if catchs an error
      await deleteUserInnitial(user.uid)
        .then(() => {
          console.log('User with id : ' + user.uid + ' deleted because of an error occured when saving data in db')
        })
        .catch(error_2 => {
          console.error('Error deleting user:', user.uid , error_2)
        })
    })
});
exports.addDefaultValuesToThePost = functions.firestore
  .document('User_Posts/{User_Posts_ID}').onCreate((snap, context) => {

    const postId = context.params.User_Posts_ID

    return addDefaultValuesToThePost(postId)
      .catch(err => {
        console.error("Error when adding new post default values")
        console.log(err)
      })
  });
