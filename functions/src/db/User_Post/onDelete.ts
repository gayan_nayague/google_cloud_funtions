import * as admin from 'firebase-admin'


export default function deleteThePost(postId: string): Promise<void | FirebaseFirestore.WriteResult> {

  return deleteCommentsOnPost(postId)
    .then(() => {
      return delPost(postId)
    })
    .catch(error => {
      console.error('Error when deleting the post ' + postId , error)
    })
};

const deleteCommentsOnPost = (postId: string) => {

  return admin
    .firestore()
    .collection("Posts_Comments")
    .where("postId", "==", postId)
    .get()
    .then(querySnapshot => {
      // console.log('Hit 2')
      // console.log('Comment size :' + querySnapshot.size)
      querySnapshot.forEach(doc => {
        doc.ref.delete()
          .catch(error => {
            console.error("Error deleting one comment",error)
          })
      })
    })
}
const delPost = (postId: string) => {

  return admin.firestore()
    .collection("User_Posts")
    .doc(postId)
    .delete()

}