import * as admin from 'firebase-admin'


export default function likeUnlikeToPost(likeBody: any): Promise<FirebaseFirestore.WriteResult> {

    if(likeBody.like){
        return likePost(likeBody.userID,likeBody.postID)       
    }else{
        return undoLikePost(likeBody.userID,likeBody.postID)
    }

}
const likePost = (userId:string,postID:string) => {
    return admin.firestore()
    .collection("User_Posts")
    .doc(postID)
    .update({
        likes : admin.firestore.FieldValue.arrayUnion(userId)
    })
}
const undoLikePost = (userId:string,postID:string) => {
    return admin.firestore()
    .collection("User_Posts")
    .doc(postID)
    .update({
        likes :  admin.firestore.FieldValue.arrayRemove(userId)
    })
}