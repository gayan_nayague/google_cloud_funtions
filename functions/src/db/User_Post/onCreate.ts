import * as admin from 'firebase-admin'
import getTime from '../../common/GetTime'

export default function addDefaultValuesToThePost(postId: string): Promise<FirebaseFirestore.WriteResult> {

    const posted_date_time_UTC = getTime()

    return admin.firestore()
        .collection('User_Posts')
        .doc(postId)
        .update({
            isVerified: false,
            likes: [],
            shares: [],
            numberOfComments: 0,
            postedTime: posted_date_time_UTC
        })

};