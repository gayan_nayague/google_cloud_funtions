import * as admin from 'firebase-admin'

export default function getCategories(): Promise<FirebaseFirestore.QuerySnapshot> {

    return admin.firestore()
    .collection('Categories')
    .get()

}