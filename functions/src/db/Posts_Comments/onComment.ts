import * as admin from 'firebase-admin'
import getTime from '../../common/GetTime'

export default function commentOnPost(comment: any): Promise<void | FirebaseFirestore.WriteResult> {

  comment.likes = []
  comment.postedTime = getTime()

  return addCommentsToDb(comment)
    .then(() => {
      return updateNumberOfCommentsOnPost(comment.postId)
    })
    .catch(error => {
      console.error('Error Commenting post')
      console.log(error);
    })

  // return addCommentsToDb(comment)
  //             .then((docRef) => {
  //                 //Update the post fields 
  //                  comment.commentID = docRef.id

  //                  getPost(comment.postId)
  //                      .then(post => {
  //                          let numberOfComments = 0

  //                          //Get current number of comments
  //                          getNumberOfComments(comment.postId)
  //                          .then(snap => {
  //                              numberOfComments = snap.size

  //                              updateFieldsOnPost(comment.postId,numberOfComments)
  //                              .catch((error) => {
  //                                  console.error("Error : Comments array not saved")
  //                                  console.log(error)
  //                              })
  //                          })
  //                          .catch(error =>{
  //                              console.error("Error while getting the number of comments")
  //                              console.log(error)
  //                          })

  //                      })
  //                     .catch(error => {
  //                         console.error("Error getting post to comment")
  //                         console.log(error)
  //                     })
  //             })
  //             .catch(error => {
  //                 console.error("Post Comment not added")
  //                 console.log(error)
  //             })

}

const addCommentsToDb = (commentBody: any) => {
  return admin.firestore()
    .collection("Posts_Comments")
    .add(commentBody)
}
// const getPost = (postID: string) => {
//   return admin.firestore()
//     .collection('User_Posts')
//     .doc(postID)
//     .get()
// }
// const getNumberOfComments = (postId: string) => {
//   return admin.firestore()
//     .collection("Posts_Comments")
//     .where("postId", "==", postId)
//     .get()
// }
const updateNumberOfCommentsOnPost = (postID: string) => {

  const increment = admin.firestore.FieldValue.increment(1);

  //numbreofComments is the current number of comments and we make an increment for it
  return admin.firestore()
    .collection('User_Posts')
    .doc(postID)
    .update({ numberOfComments: increment })
}