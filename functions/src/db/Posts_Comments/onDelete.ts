import * as admin from 'firebase-admin'

export default function commentOnDelete(commentId: string, postId: string): Promise<void | FirebaseFirestore.WriteResult> {

  //console.log('Hit');

  return deleteCommentFromDb(commentId)
    .then(() => {
      return updateCommentCountInDb(postId)
    }).catch(error => {
      console.error('Error delting comment_1')
      console.log(error)
    })

}
const deleteCommentFromDb = (commentId: string) => {

  //console.log(commentId);

  return admin.firestore()
    .collection("Posts_Comments")
    .doc(commentId)
    .delete()
}
const updateCommentCountInDb = (postId: string) => {

  //console.log(postId);

  const decrement = admin.firestore.FieldValue.increment(-1)

  return admin.firestore()
    .collection("User_Posts")
    .doc(postId)
    .update({ numberOfComments: decrement })

}
