import * as admin from 'firebase-admin'

export default function likeOnComment(likeBody: any): Promise<FirebaseFirestore.WriteResult> {

    if (likeBody.like) {
        return likeComment(likeBody.userID, likeBody.postID, likeBody.commentID)
    } else {
        return undoLikeComment(likeBody.userID, likeBody.postID, likeBody.commentID)
    }

}

const likeComment = (userId:string, postID:string, commentID:string) => {
    
    return admin.firestore()
    .collection("Posts_Comments")
    .doc(commentID)
    .update({
        likes : admin.firestore.FieldValue.arrayUnion(userId)
    })
}
const undoLikeComment = (userId:string, postID:string, commentID:string) => {
    return admin.firestore()
    .collection("Posts_Comments")
    .doc(commentID)
    .update({
        likes :  admin.firestore.FieldValue.arrayRemove(userId)
    })
}