import * as admin from 'firebase-admin'

export default function onProfilePictureUpload(uId: string): any {

    return getAllPostsIDs(uId);
             
};

const getAllPostsIDs = (uId:string) => {
    
    return admin.firestore()
    .collection("User_Posts")
    .select()
    .where("userId","==",uId)
    
}